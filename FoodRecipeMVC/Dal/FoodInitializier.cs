﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using FoodRecipeMVC.Models;
namespace FoodRecipeMVC.Dal
{
    public class FoodInitializer : DropCreateDatabaseIfModelChanges<RecipesContext>
    {
        protected override void Seed(RecipesContext context)
        {
            var recipes = new List<Recipe>()
            {
                new Recipe {RId=1,KindOfRecipe = "Soups", Image="/Content/1.jpg"},
                new Recipe {RId=2, KindOfRecipe = "Salad", Image="/Content/2.jpg"},
                new Recipe {RId=3,KindOfRecipe = "Main Dish",Image="/Content/3.jpg"},
                new Recipe {RId=4, KindOfRecipe = "Breakfast",Image="/Content/4.jpg" },
                new Recipe {RId=5,KindOfRecipe = "Drinks",Image="/Content/5.jpg"},
                new Recipe {RId=6, KindOfRecipe = "Desserts",Image="/Content/6.jpg" },
                new Recipe {RId=7,KindOfRecipe = "Fish & Sea food",Image="/Content/7.jpg"},
               
        
        
            };
            recipes.ForEach(r => context.Recipes.Add(r));
            context.SaveChanges();
        }
    }
}