﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FoodRecipeMVC.Models;
using System.IO;
using System.Web.Helpers;
using System.Web.Routing;
using System.Web.Security;
using FoodRecipeMVC.Dal;

namespace FoodRecipeMVC.Controllers
{
    public class Default1Controller : Controller
    {
        private RecipesContext db = new RecipesContext();

        public ActionResult IndexImage()
        {
            using (var db = new RecipesContext())
            {
                return PartialView(db.Recipes.ToList());
                   
            }
        }
        public ViewResult Index()
        {
            using (var db = new RecipesContext())
            {
                return View(db.Recipes.ToList());
            }
        }
      
          [Authorize]
        public ActionResult Browse(string kindOfRecipe)
        {
              if (!User.Identity.IsAuthenticated)
              { 
                  return RedirectToAction("Login");
              }
            else
            {
                using (var db = new RecipesContext())
                {
                    var recipes = db.Recipes.Include("RecipeDetails").Single(r => r.KindOfRecipe == kindOfRecipe);
                    List<RecipeDetails> rd = new List<RecipeDetails>();
                    recipes.RecipeDetails.ForEach(r => rd.Add(r));
                    return View(rd);
                }
            }
        }
      [ChildActionOnly]
        public ActionResult RecipeMenu()
        {
            using (var db = new RecipesContext())
            {
                var genres = db.Recipes.ToList();
                return PartialView(genres);
            }
        }

      public ActionResult RecipeComment(int RDId)
      {
          using (var db = new RecipesContext())
          {

              var comm = db.RecipeDetails.Include("Comments").Single(c => c.RDId == RDId);
              List<Comment> comment = new List<Comment>();
              comm.Comments.ForEach(com => comment.Add(com));
              return PartialView(comment);
          }
      }

       

        public ViewResult Details(int RDid)
        {
            RecipeDetails recipe = db.RecipeDetails.Find(RDid);
            return View(recipe);
        }
         [Authorize]
        public ActionResult Create()
        {
            ViewBag.RId = new SelectList(db.Recipes, "RId", "KindOfRecipe");
               
            return View();
        } 
        public void Upload(RecipeDetails reciped)
        {
            var image = WebImage.GetImageFromRequest();
            var filename = Path.GetFileName(image.FileName);
            var path = Path.Combine(Server.MapPath("~/Content/Pictures"), filename);
            image.Save(path);
            reciped.ImageUrl= Url.Content(Path.Combine("~/Content/Pictures", filename));
        }
        [HttpPost]
        [Authorize]
        public ActionResult Create(RecipeDetails reciped)
        {
            if (!User.Identity.IsAuthenticated)
            { return RedirectToAction("Login"); }
            else
            {
                using (var db = new RecipesContext())
                {
                  
                    if (ModelState.IsValid)
                    {
                        Upload(reciped);
                        reciped.Date = DateTime.Now;
                        db.RecipeDetails.Add(reciped);
                        db.SaveChanges();

                        return RedirectToAction("Index");
                    }
                    ViewBag.RId = new SelectList(db.Recipes, "RId", "KindOfRecipe", reciped.RId);
                }
            
                return View(reciped);
            }
        }

         [Authorize]
        public ActionResult AddComment(Comment comment)
        {
            if (!User.Identity.IsAuthenticated)
            { return RedirectToAction("Login"); }
            else
            {
                using (var db = new RecipesContext())
                {

                    if (ModelState.IsValid)
                    {





                        if (comment.Description != "" || comment.SubmitedFrom != " ")
                        {
                            comment.SubmitedOn = DateTime.Now;
                            db.Comment.Add(comment);
                            db.SaveChanges();
                        }
                    }
               }

                return PartialView();
            }   
        }
       
         public ActionResult BrowseTop10()
         {
                 using (var db = new RecipesContext())
                 {
                     List<RecipeDetails> rd = new List<RecipeDetails>();
                    
                     int pom = db.RecipeDetails.Count();
                     if (pom > 10)
                     {
                         int pom1 = pom - 10;
                        
                         while (pom > pom1)
                         {
                             rd.Add(db.RecipeDetails.Single(r => r.RDId == pom));
                             pom = pom - 1;
                            
                         }

                     }
                     else
                     {
                         while (pom > 0)
                         {
                             rd.Add(db.RecipeDetails.Single(r => r.RDId == pom));
                             pom = pom - 1;
                         }
                     }
                     return PartialView(rd);
                 }
             
         }
        [Authorize]
         public ActionResult BrowseTop10inRecipe(string KindOfRecipe)
         {
             
             using (var db = new RecipesContext())
             {
                
                 var recipes = db.Recipes.Include("RecipeDetails").Single(r => r.KindOfRecipe == KindOfRecipe);
                 
                 List<RecipeDetails> rd = new List<RecipeDetails>();
                 List<RecipeDetails> rd1 = new List<RecipeDetails>();
                 recipes.RecipeDetails.ForEach(r => rd.Add(r));
                 rd = rd.OrderByDescending(r => r.RDId).ToList();
                 ViewBag.KindOfRecipe = KindOfRecipe;
                 int pom = rd.Count();
                 if (pom > 10)
                 {
                     int pom1 = pom - 10;
                     rd1.AddRange(rd.Take(pom1));
                     
                     }

                 else
                 {
                     rd1.AddRange(rd.Take(pom));

                    
                 }
                 return PartialView(rd);
             }

         }

        public ActionResult SearchIndex(string KindOf, string searchName)
        {
            using (var db = new RecipesContext())
            {


                var kindOfRecipe = new List<string>();

                var RecipeQry = from d in db.Recipes
                                select d.KindOfRecipe;
                kindOfRecipe.AddRange(RecipeQry);
                ViewBag.KindOf = new SelectList(kindOfRecipe);

                var recipe = from m in db.RecipeDetails select m;


                if ((!String.IsNullOrEmpty(searchName)) && (!String.IsNullOrEmpty(KindOf)))
                {
                    recipe = recipe.Where(s => s.Name.Contains(searchName) && s.Recipe.KindOfRecipe == KindOf);



                    return View(recipe.ToList());
                }
                else if (!String.IsNullOrEmpty(searchName))
                {
                    recipe = recipe.Where(s => s.Name.Contains(searchName));
                    return View(recipe.ToList());

                }
                else if (!String.IsNullOrEmpty(KindOf))
                {
                    return View(recipe.Where(x => x.Recipe.KindOfRecipe == KindOf).ToList());
                }
                else return View(recipe.ToList());

            }
        }

    }
}
