﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace FoodRecipeMVC.Models
{
    public partial class Recipe
    {
        [Key]
        public int RId { get; set; }
        public string KindOfRecipe { get; set; }
        public string Image { get; set; }
        public List<RecipeDetails> RecipeDetails { get; set; }
    }
}


