﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace FoodRecipeMVC.Models
{
    public class Comment
    {
        [Key]
        public int comID { get; set; }
        [Required(ErrorMessage = "Please leave a comment")]
        [DisplayName("Comment")]
        public string Description { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [DisplayName("Submited On")]
        public DateTime SubmitedOn { get; set; }
        [Required(ErrorMessage = "Please fill your name")]
        [DisplayName("Submited From")]
        public string SubmitedFrom { get; set; }
        [DisplayName("RecipeDetails")]
        public int RDId { get; set; }
        public virtual RecipeDetails RecipeDetail { get; set; }
       
    
        


        
       

    }
}