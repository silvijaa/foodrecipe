﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc.Razor;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace FoodRecipeMVC.Models
{
    public class RecipeDetails
    {
        [Key]
        public int RDId { get; set; }
        [Required(ErrorMessage = "An Recipe Name is required")]
        [DisplayName("Recipe Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Ingredients are required")]
        [DisplayName("What We Need")]
        public string WhatWeNeed { get; set; }
        [Required(ErrorMessage = "How to made steps are required")]
        [DisplayName("How To Made")]
        public string HowToMade { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [DisplayName("Submited On")]
        public DateTime Date { get; set; }
        [UIHint("ProfileImage")]
        [StringLength(1024)]
        public string ImageUrl { get; set; }
        [DisplayName("First Name")]
        [Required(ErrorMessage = "First name is required")]
        public string FName { get; set; }
        [DisplayName("Last Name")]
        public string LName { get; set; }
        [DisplayName("From")]
        public string City { get; set; }
        [DisplayName("Recipe")]
        public int RId { get; set; }
        public virtual Recipe Recipe { get; set; }
        public List<Comment> Comments { get; set; }






    }
}