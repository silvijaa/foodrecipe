﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
namespace FoodRecipeMVC.Models
{
    public class RecipesContext : DbContext
    {
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<RecipeDetails> RecipeDetails { get; set; }
        public DbSet<Comment> Comment { get; set; }
    }
}